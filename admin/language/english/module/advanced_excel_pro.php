<?php

$_['heading_title'] = 'Advanced Excel Pro';

$_['text_module'] = 'Modules';
$_['text_file_noselected'] = 'File not selected';
$_['text_form_changed'] = 'Change data';
$_['text_provider_noselected'] = 'Not selected';
$_['text_error_extension'] = 'File extension is not valid. Only files with extension .xls or .xlsx. are allowed';
$_['text_new_provider'] = 'Create new provider';
$_['text_create'] = 'Create';
$_['text_noselected'] = 'Not selected';

$_['error_provider'] = 'Enter the name of provider';

$_['tab_import'] = 'Import';
$_['tab_export'] = 'Export';

$_['button_download'] = 'Download file';
$_['button_save_areas'] = 'Save set of fields';

$_['entry_provider'] = 'Provider';
$_['entry_img_title'] = 'Image title';
$_['entry_model_code'] = 'Model/Vendor code';
$_['entry_title'] = 'Title';
$_['entry_description'] = 'Description';
$_['entry_manufacturer'] = 'Manufacturer';
$_['entry_category'] = 'Category';
$_['entry_model_attr'] = 'Model';
$_['entry_size'] = 'Size';
$_['entry_cloth'] = 'The cloth';
$_['entry_pillowcases'] = 'Pillowcases';
$_['entry_filler'] = 'Filler';
$_['entry_sheets'] = 'Sheets';
$_['entry_price'] = 'Price';