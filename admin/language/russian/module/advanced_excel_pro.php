<?php

$_['heading_title'] = 'Advanced Excel Pro';

$_['text_module'] = 'Модули';
$_['text_file_noselected'] = 'Файл не выбран';
$_['text_form_changed'] = 'Изменить данные';
$_['text_provider_noselected'] = 'Не выбрано';
$_['text_error_extension'] = 'Неверный формат файла. Допускаются только файлы с расширением .xls или .xlsx.';
$_['text_new_provider'] = 'Создать нового поставщика';
$_['text_create'] = 'Создать';
$_['text_noselected'] = 'Не выбрано';

$_['error_provider'] = 'Введите название поставщика';

$_['tab_import'] = 'Импорт';
$_['tab_export'] = 'Экспорт';

$_['button_download'] = 'Загрузить файл';
$_['button_save_areas'] = 'Сохранить набор полей';

$_['entry_provider'] = 'Поставщик';
$_['entry_img_title'] = 'Название картинки';
$_['entry_model_code'] = 'Модель/артикул';
$_['entry_title'] = 'Наименование';
$_['entry_description'] = 'Описание';
$_['entry_manufacturer'] = 'Производитель';
$_['entry_category'] = 'Категория';
$_['entry_model_attr'] = 'Модель';
$_['entry_size'] = 'Размер';
$_['entry_cloth'] = 'Ткань';
$_['entry_pillowcases'] = 'Наволочки';
$_['entry_filler'] = 'Наполнитель';
$_['entry_sheets'] = 'Простынь';
$_['entry_price'] = 'Цена';