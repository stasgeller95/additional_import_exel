<?php echo $header; ?>
<div id="content">
    <div class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <?php echo $breadcrumb['separator']; ?><a
            href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
        <?php } ?>
    </div>
    <h1><?php echo $heading_title; ?></h1>
    <div class="container-fluid">
        <div class="panel panel-default">
            <div class="panel-body">
                <ul class="nav nav-tabs">
                    <li><a class="selected" href="#tab-advanced-import" data-toggle="tab"><?php echo $tab_import; ?></a>
                    </li>
                    <li><a href="#tab-advanced-export" data-toggle="tab"><?php echo $tab_export; ?></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="tab-advanced-import" style="display: block;">
                        <form enctype="multipart/form-data" method="POST" action="<?php echo $provider_url; ?>">
                            <label class="download-excel">
                                <button type="button"><?php echo $button_download; ?></button>
                                <mark><?php echo $text_file_noselected ?></mark>
                                <input type="file" name="Excel" value="<?php echo $button_download; ?>">
                            </label>
                            <span class="error_extension"><?php echo $text_error_extension; ?></span>
                        </form>
                        <form action="<?php echo $import_url;?>" id="provider" method="GET" class="uploading-data">
                            <h2><?php echo $text_form_changed; ?></h2>
                            <div class="form-provider">
                                <label for="select_provider"><?php echo $entry_provider; ?></label>
                                <select name="provider" id="select_provider">
                                    <option value="null"><?php echo $text_provider_noselected; ?></option>
                                    <option value="new_provider"><?php echo $text_new_provider; ?></option>
                                    <?php if ($providers) {
                                        foreach ($providers as $provider): ?>
                                            <option value="<?php echo $provider['provider_id']; ?>"><?php echo $provider['name']; ?></option>
                                        <?php endforeach;
                                    } ?>
                                </select>
                                <div class="new-provider">
                                    <button type="button" id="create_provider"><?php echo $text_create; ?></button>
<!--                                    <input type="submit" id="create_provider" value="--><?php //echo $text_create; ?><!--">-->
                                    <input type="text" name="provider" placeholder="Название поставщика">
                                    <input type="hidden" id="token" value="<?php echo $token; ?>">
                                    <span class="error"><?php echo $error_provider; ?></span>
                                </div>
                            </div>
                            <table class="excel-data-table">
                                <tr>
                                    <td><?php echo $entry_img_title; ?></td>
                                    <td>
                                        <select name="img_title" id="img_title">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_model_code; ?></td>
                                    <td>
                                        <select name="model" id="model">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_title; ?></td>
                                    <td>
                                        <select name="title" id="title">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_description; ?></td>
                                    <td>
                                        <select name="description" id="description">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_manufacturer; ?></td>
                                    <td>
                                        <select name="manufacturer" id="manufacturer">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_category; ?></td>
                                    <td>
                                        <select name="category" id="category">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_model_attr; ?></td>
                                    <td>
                                        <select name="model_attr" id="model_attr">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_size; ?></td>
                                    <td>
                                        <select name="size" id="size">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_cloth; ?></td>
                                    <td>
                                        <select name="cloth" id="cloth">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_pillowcases; ?></td>
                                    <td>
                                        <select name="pillowcases" id="pillowcases">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_filler; ?></td>
                                    <td>
                                        <select name="filler" id="filler">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_sheets; ?></td>
                                    <td>
                                        <select name="sheets" id="sheets">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><?php echo $entry_price; ?></td>
                                    <td>
                                        <select name="price" id="price">
                                            <option value="0"><?php echo $text_noselected; ?></option>
                                            <?php for($i = 0; $i < count($columns); $i++): ?>
                                                <option value="<?php echo $columns[$i];?>"><?php echo $columns[$i];?></option>
                                            <?php endfor;?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <div class="form-buttons">
                                <input type="submit" id="save-fields" value="<?php echo $button_save_areas; ?>">
                                <input type="submit" value="Добавить как новый товар">
                                <input type="submit" value="Редактировать по названию">
                                <input type="submit" value="Загрузить прайс поставщика">
                                <input type="submit" value="Загрузить товары без категорий">
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tab-advanced-export" style="display: none;">
                        345
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $('.nav-tabs a').tabs();
</script>
<?php echo $footer ?>
