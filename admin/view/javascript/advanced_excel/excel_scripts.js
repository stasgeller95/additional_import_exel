$(document).ready(function () {

    var wrapper = $(".download-excel"),
        inp = wrapper.find("input"),
        btn = wrapper.find("button"),
        lbl = wrapper.find("mark");
    btn.focus(function () {
        inp.focus()
    });

    // Crutches for the :focus style:
    inp.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    btn.add(lbl).click(function () {
        inp.click();
    });

    btn.focus(function () {
        wrapper.addClass("focus");
    }).blur(function () {
        wrapper.removeClass("focus");
    });

    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    inp.change(function () {
        var file_name;
        var file_extension;
        if (file_api && inp[0].files[0]) {
            file_name = inp[0].files[0].name;
            file_extension = inp.val().split('.').pop();
        }
        else
            file_name = inp.val().replace("C:\\fakepath\\", '');

        if (!file_name.length)
            return;

        if (file_extension == 'xls' || file_extension == 'xlsx') {
            $('.error_extension').css('display', 'none');
        } else {
            $('.error_extension').css('display', 'block');
            return false;
        }

        if (lbl.is(":visible")) {
            lbl.text(file_name);
            $('.uploading-data').css('display', 'block');
        } else
            btn.text(file_name);
    }).change();

    var select_provider = $('#select_provider');

    select_provider.change(function () {

        if (select_provider.val() == 'new_provider') {
            $('.new-provider').css('display', 'block');
        } else if (select_provider.val() == 'null') {
            $('#provider').trigger('reset');
        } else {
            $('.new-provider').css('display', 'none');

            var provider_id = select_provider.val();
            if (provider_id == 'null') {
                return false;
            }

            var token = $('#token').val();

            $.ajax({
                url: '/admin/index.php?route=module/advanced_excel_pro/getProviderData&token=' + token,
                type: 'post',
                dataType: 'json',
                data: {
                    provider_id: provider_id
                },
                success: function (provider_data) {

                    var data = deserialize(provider_data);

                    for (var key in data) {
                        var selects = document.getElementById(key);

                        for (var i = 0; i < selects.length; i++) {
                            var opt = selects[i];

                            if (data[key] == $(opt).val()) {
                                console.log(opt.selected);
                                opt.selected = true;
                            } else {
                                $(selects).defaultSelected;
                            }
                        }
                    }
                },
                error: function (provider_data) {
                    console.log('error');
                }

            });
        }
    }).change();

    // create new provider

    $('#create_provider').on('click', function () {
        var provider_val = $(this).next().val();
        var token = $('#token').val();

        if (provider_val == '') {
            $(this).next().next().css('display', 'block');
            return false;
        } else {
            $(this).next().next().css('display', 'none');
        }

        $.ajax({
            url: '/admin/index.php?route=module/advanced_excel_pro/create_provider&token=' + token,
            type: 'POST',
            dataType: 'json',
            data: {provider: provider_val},
            success: function (id) {
                location.reload();
            },
            error: function (id) {
                alert('Такой поставщик уже существует');
            }
        });

        return false;
    });

    // save set of fields

    $('#save-fields').on('click', function () {
        var provider_id = $('#select_provider').val();
        var token = $('#token').val();
        var select_provider = $('.excel-data-table select').serialize();


        if (provider_id == 'null') {
            return false;
        }

        $.ajax({
            url: '/admin/index.php?route=module/advanced_excel_pro/setFieldsById&token=' + token,
            type: 'POST',
            dataType: 'json',
            data: {
                provider_id: provider_id,
                data_provider: select_provider

            },
            success: function (last_id) {
                console.log('success');
            },
            error: function (last_id) {
                console.log('error');
            }
        });

        return false;
    });

    function deserialize(serializedString) {
        var str = decodeURI(serializedString);
        var pairs = str.split('&amp;');
        var obj = {}, p, idx, val;
        for (var i = 0, n = pairs.length; i < n; i++) {
            p = pairs[i].split('=');
            idx = p[0];

            if (idx.indexOf("[]") == (idx.length - 2)) {

                var ind = idx.substring(0, idx.length - 2)
                if (obj[ind] === undefined) {
                    obj[ind] = [];
                }
                obj[ind].push(p[1]);
            }
            else {
                obj[idx] = p[1];
            }
        }
        return obj;
    }
});