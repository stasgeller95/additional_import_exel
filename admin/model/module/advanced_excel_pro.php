<?php
class ModelModuleAdvancedExcelPro extends Model
{
    public function addProvider($provider){

        $this->db->query("insert into ". DB_PREFIX ."provider set name='". $provider ."'");

        return $this->db->getLastId();
    }

    public function getProviders(){
        $query = $this->db->query("select * from ". DB_PREFIX ."provider");

        return $query->rows;
    }

    public function getFieldsFromProviderId($provider_id){
        $query = $this->db->query("select data from ". DB_PREFIX ."provider where provider_id =". $provider_id ."");

        if(is_null(end($query->row))){

            $data = array(
                'img_title' => 0,
                'model' => 0,
                'title' => 0,
                'description' => 0,
                'manufacturer' => 0,
                'category' => 0,
                'model_attr' => 0,
                'size' => 0,
                'cloth' => 0,
                'pillowcases' => 0,
                'filler' => 0,
                'sheets' => 0,
                'price' => 0,
            );

            return json_encode($data);
        } else{
            $fills = end($query->row);

            return $fills;
        }
    }

    public function setFields($provider_id, $data){

        $this->db->query("update ". DB_PREFIX ."provider set data = '".$data."' where provider_id = '" . $provider_id ."'");

        return $this->db->getLastId();
    }
}