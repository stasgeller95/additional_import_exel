<?php

class ControllerModuleAdvancedExcelPro extends Controller
{
    private $error = array();

    public function index(){
        $this->data = array();

        $this->load->language('module/advanced_excel_pro');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->document->addStyle('view/stylesheet/advanced_excel_pro.css');
        $this->document->addScript('view/javascript/advanced_excel/excel_scripts.js');

        $this->data['breadcrumbs'] = array(
            array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => false
            ),
            array(
                'text'      => $this->language->get('text_module'),
                'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            ),
            array(
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('module/excelp', 'token=' . $this->session->data['token'], 'SSL'),
                'separator' => ' :: '
            )
        );

        $this->data['action'] = $this->url->link('module/advanced_excel_pro', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['export_url'] = $this->url->link('module/advanced_excel_pro',  'token=' . $this->session->data['token'] . '&action=export', 'SSL');
        $this->data['import_url'] = $this->url->link('module/advanced_excel_pro',  'token=' . $this->session->data['token'] . '&action=import', 'SSL');
        $this->data['provider_url'] = $this->url->link('module/advanced_excel_pro',  'token=' . $this->session->data['token'] . '&action=provider', 'SSL');

        $this->data['token'] = $this->session->data['token'];

        $this->load->model('module/advanced_excel_pro');
        $this->load->model('setting/store');
        $this->load->model('setting/setting');
        $this->load->model('localisation/language');


        $this->data['heading_title'] = $this->language->get('heading_title');

        $this->data['text_file_noselected'] = $this->language->get('text_file_noselected');
        $this->data['text_form_changed'] = $this->language->get('text_form_changed');
        $this->data['text_provider_noselected'] = $this->language->get('text_provider_noselected');
        $this->data['text_error_extension'] = $this->language->get('text_error_extension');
        $this->data['text_new_provider'] = $this->language->get('text_new_provider');
        $this->data['text_create'] = $this->language->get('text_create');
        $this->data['text_noselected'] = $this->language->get('text_noselected');

        $this->data['tab_import'] = $this->language->get('tab_import');
        $this->data['tab_export'] = $this->language->get('tab_export');

        $this->data['button_download'] = $this->language->get('button_download');
        $this->data['button_save_areas'] = $this->language->get('button_save_areas');

        $this->data['error_provider'] = $this->language->get('error_provider');

        $this->data['entry_provider'] = $this->language->get('entry_provider');
        $this->data['entry_img_title'] = $this->language->get('entry_img_title');
        $this->data['entry_model_code'] = $this->language->get('entry_model_code');
        $this->data['entry_title'] = $this->language->get('entry_title');
        $this->data['entry_description'] = $this->language->get('entry_description');
        $this->data['entry_manufacturer'] = $this->language->get('entry_manufacturer');
        $this->data['entry_category'] = $this->language->get('entry_category');
        $this->data['entry_model_attr'] = $this->language->get('entry_model_attr');
        $this->data['entry_size'] = $this->language->get('entry_size');
        $this->data['entry_cloth'] = $this->language->get('entry_cloth');
        $this->data['entry_pillowcases'] = $this->language->get('entry_pillowcases');
        $this->data['entry_filler'] = $this->language->get('entry_filler');
        $this->data['entry_sheets'] = $this->language->get('entry_sheets');
        $this->data['entry_price'] = $this->language->get('entry_price');

        $this->load->model('catalog/manufacturer');

        $providers_info = $this->model_module_advanced_excel_pro->getProviders();

        $this->data['providers'] = array();
        if($providers_info){
            foreach ($providers_info as $provider){

                $this->data['providers'][] = array(
                    'provider_id' => $provider['provider_id'],
                    'name' => $provider['name'],
                    'data' => $provider['data']
                );
            }
        }

        $this->data['columns'] = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' , 'K', 'L', 'M');
//        var_dump(json_encode($this->data['columns'])); die();

        if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }

        $this->response->addHeader('Cache-Control: no-cache, no-store');

        $this->children = array(
            'common/header',
            'common/footer'
        );
        $this->template = 'module/advanced_excel_pro.tpl';

        $this->response->setOutput($this->render());
    }

    public function create_provider(){

        $this->load->model('module/advanced_excel_pro');

        if ($this->request->post['provider']){

            $this->model_module_advanced_excel_pro->addProvider($this->request->post['provider']);

        } else{
            return false;
        }
    }

    public function getProviderData(){
        $this->load->model('module/advanced_excel_pro');

        if ($this->request->post['provider_id']){
            $data = $this->model_module_advanced_excel_pro->getFieldsFromProviderId($this->request->post['provider_id']);

            $this->response->setOutput(json_encode($data));
        } else{
            return false;
        }
    }

    public function setFieldsById(){
        $this->load->model('module/advanced_excel_pro');

        if($this->request->post['provider_id'] && $this->request->post['data_provider']){

            $this->model_module_advanced_excel_pro->setFields($this->request->post['provider_id'], $this->request->post['data_provider']);
        }
    }
}